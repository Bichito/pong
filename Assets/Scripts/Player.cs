﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	private Transform playerTransform;

	private Vector3 newPos;

	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
		/*
		//Read input
		move = Input.GetAxis("Vertical");


		//Apply speed
		move *= speed;


		//Limit position
		newPos = new Vector3 (newPos.x, playerTransform.position.y + move, newPos.z );

		if (newPos.y < -4.4f) 
		{
			newPos = new Vector3 (newPos.x, -4.4f, newPos.z);
		}
		if (newPos.y > 4.4f) 
		{
			newPos = new Vector3 (newPos.x, 4.4f, newPos.z);
		}

		//Set new position
		playerTransform.position = newPos;
		*/

		move = Input.GetAxis("Vertical") * speed;
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(0, move, 0);

		if (transform.position.y < -3) {
			transform.position = new Vector3 (transform.position.x, -3, transform.position.z);
		}else if(transform.position.y > 6) {
			transform.position = new Vector3 (transform.position.x, 6, transform.position.z);
		}
	}
}
